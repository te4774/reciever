#!/bin/bash
 
#if [[ $# -ne 4 ]]; then
#    echo "Syntax: $0 <source login> <source password> <target login> <target password>"
#    exit 2
#fi
 
# for safety reasons, repositories are hardcoded here, so we are sure we will not accidentally overwrite some unexpected repository by providing wrong arguments on the command line
# for security reasons, the logins/passwords are stored somewhere else
 

source_login="$1"
source_password="$2"
target_login="$1"
target_password="$2"
source_repo="$3"
source_project="$4"
target_repo="$5"
target_project="$6"
 
echo "=== cloning https://$source_repo/$source_project.git"
git clone --mirror https://$source_login:$source_password@$source_repo/$source_project.git
if [[ $? -ne 0 ]]; then
    echo "!! Failed to clone https://$source_repo/$source_project.git"
    echo "=== done"
    exit 2
fi
 
echo "=== cleaning up refs"
cd $source_project.git
git show-ref | grep ' refs/merge-requests/' | cut -d' ' -f2 | xargs -n1 -r git update-ref -d # do not push merge-requests refs to GitLab, it would refuse them
 
echo "=== pushing to https://$target_repo/$target_project.git"
git push --mirror https://$target_login:$target_password@$target_repo/$target_project.git
if [[ $? -ne 0 ]]; then
    echo "!! Failed to push to https://$target_repo/$target_project.git"
    # keep running the script to delete local repository
fi
 
echo "=== removing local repository"
cd ..
rm -fr $source_project.git
 
echo "=== done"
