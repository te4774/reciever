#!/bin/bash

DISTRO_DEPLOY_REPO=""
RAW_ACCEPTANCE_INTERNAL_REPO="REPO privé"
RAW_ACCEPTANCE_PUBLIC_REPO="REPO publique"
DISTRO_DEPLOY_PATH="/tm/plugin-premium/toto/titi"
CI_COMMIT_TAG="v5.2.0-rc3"
ARTIFACT_VISIBILITY="private"
REGEX="^v(\d+)\.(\d+)\.(\d+)-(rc|it)(\d+)($|-.+)"
pat="[:alnum:]"
#
if [[ $CI_COMMIT_TAG =~ $pat ]] && [[ $ARTIFACT_VISIBILITY == "private" || ${DISTRO_DEPLOY_PATH} =~ /plugin-premium/ ]];
then
        DISTRO_DEPLOY_REPO=$RAW_ACCEPTANCE_INTERNAL_REPO
        echo "$DISTRO_DEPLOY_REPO"

else # [[ $CI_COMMIT_TAG =~ /^v(\d+)\.(\d+)\.(\d+)-(rc|it)(\d+)($|-.+)/ ]] && [[ $ARTIFACT_VISIBILITY == "public" && ${DISTRO_DEPLOY_PATH} != "plugin-premium" ]]
        DISTRO_DEPLOY_REPO=$RAW_ACCEPTANCE_PUBLIC_REPO
        echo "$DISTRO_DEPLOY_REPO"
fi


if [[ "v5.2.0-rc3" =~ $pat ]] && [[ true ]]; then echo "ok";
fi